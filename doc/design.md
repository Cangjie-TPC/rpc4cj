# rpc4cj

过程远程调用，通过网络调用远程对象，业界主流框架有 gRPC, Apache Thrift, gRPC 需要 http2.0 ,在 ProtoBuf 上增加仓颉语言支持， 然后集成 gRPC 框架，参考 js 封装1614 行。


#### 主要类:

#### class Server
服务器是用于服务RPC请求的gRPC服务器。

```cangjie
public class Server {
    
    /**
     * 根据可选择参数，创建 Server 实例。
     * 参数 opts - Server的服务器选项设置类
     * 参数 lis - HashMap 数据结构， key 为 GRPCServerSocket, value 为 Bool 类型
     * 参数 conns - 包含所有活动服务器传输。它是一个映射，key 为监听地址，值是属于该侦听器的活动传输集。
     * 参数 server - Bool 数据类型，是否已经启动服务
     * 参数 drain - Bool 数据类型，是否已经排空
     * 参数 services - HashMap 数据结构， key 为 service 名称, value 为有关服务的信息类 ServiceInfoInside 数据类型
     * 参数 czData - 用于存储ClientConn、addrConn和Server的channelz相关数据
     */
    public init(opts!: ServerOptions = ServerOptions(),
            lis!: HashMap<GRPCServerSocket, Bool> = HashMap<GRPCServerSocket, Bool>(),
            conns!: HashMap<String, HashMap<ServerTransport, Bool>> = HashMap<String, HashMap<ServerTransport, Bool>>(),
            server!: Bool = false,
            drain!: Bool = false,
            services!: HashMap<String, ServiceInfoInside> = HashMap<String, ServiceInfoInside>(),
            czData!: ChannelzDataInside = ChannelzDataInside())

    /**
     * 创建了一个 gRPC 服务器，该服务器没有注册服务，也没有开始接受请求。
     * 参数 opts - Array 数据结构， 服务器选项设置诸如凭据、编解码器和保留活动参数等选项
     * 返回值 Server - Server 实例
     */
    public static func newServer(opt: Array<ServerOption>): Server
    
    /**
     * 向gRPC服务器注册服务及其实现。这必须在调用服务之前调用。
     * 参数 desc - RPC 服务的规范
     * 参数 ss - Message 数据结构，可以是实际应用中自行实现的protobuf实现类
     * 注意：参数 ss 在当前版本支持的参数类型为MessageLite，后续版本更迭会修改参数类型为Any
     */
    public func registerService(desc: ServiceDesc, ss: Message): Unit
    
    /**
     * 返回从服务名称到服务信息的映射。服务名称包括包名称，形式为<package>、<Service>。
     * 返回值 HashMap<String, ServiceInfo> - HashMap 数据结构， key 为 service 名称, value 为 ServiceInfo 数据类型
     */
    public func getServiceInfo(): HashMap<String, ServiceInfo>
    
    /**
     * 接受监听器lis上的传入连接，为每个连接创建一个新的ServerTransport和service 数据处理线程。
     * 在数据处理线程中，读取gRPC请求，然后调用注册的处理程序来回复它们。
     * 当lis接受失败并出现致命错误时，Serve返回。此方法返回时，lis将关闭。
     * 除非调用Stop或GracefulStop，否则Serve将返回非零错误。
     * 参数 ss - GRPCServerSocket 数据结构， 成员变量 ss 中封装 SocketServer/TlsSocketServer
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public func serve(ss: GRPCServerSocket): GrpcError
    
    /**
     * ServeHTTP通过响应gRPC请求r，通过在gRPC服务器s中查找请求的gRPC方法，实现了Go标准库的http处理程序接口。
     * 提供的HTTP请求必须通过HTTP/2连接到达。当使用Go标准库的服务器时，实际上这意味着请求也必须通过TLS到达。
     * 要在gRPC和现有http处理程序之间共享一个端口（如https的443），请使用根http处理程序，如：
     *    if r.ProtoMajor == 2 && strings.HasPrefix(
     *        r.Header.Get("Content-Type"), "application/grpc") {
     *        grpcServer.ServeHTTP(w, r)
     *    } else {
     *        yourMux.ServeHTTP(w, r)
     *    }
     * 请注意，ServeHTTP使用Go的HTTP/2服务器实现，它与grpc Go的HTTP/2服务器完全分离。
     * 两条路径之间的性能和特性可能有所不同。ServeHTTP不支持通过GRPCGo的HTTP/2服务器提供的一些gRPC功能。
     *
     * 参数 w - ResponseWriteStream 接口，用于处理响应信息。
     * 参数 r - Request 数据结构
     * 备注：参数 r 必须同时符合以下3个条件：
     *    1. Request.protoMajor = 2；
     *    2. 必须为 POST 请求；
     *    3. Request.header 必须包含 key = "Content-Type", 且 value 值的第一个为 "application/grpc" 的键值对。
     * 备注：若参数 r 未同时符合以上3个条件，代码逻辑跳过本函数，对外不感知：
     */
    public func serveHTTP(w: ResponseWriteStream, r: Request): Unit
   
    /**
     * 停止停止gRPC服务器。它会立即关闭所有打开的连接和侦听器。
     * 它将取消服务器端的所有活动RPC，客户端上相应的挂起RPC将收到连接错误通知。
     */
    public func stop(): Unit

    /**
     * 优雅地停止gRPC服务器。它将停止服务器接受新的连接、RPC和块，直到所有挂起的RPC完成。
     */
    public func gracefulStop(): Unit

}
```

#### interface ServerOption
服务器选项设置诸如凭据、编解码器和保留活动参数等选项。

```cangjie
public interface ServerOption {
    /**
     * 传入参数so，执行apply函数。
     * 参数 so - Server的服务器选项设置类
     */
    func apply(so: ServerOptions): Unit
}
```

#### ServerOption
调用对应函数，对服务器选项设置诸如凭据、编解码器和保留活动参数等选项进行赋值。

```cangjie
/**
 * 返回一个 ServerOption，该选项指定流式RPC的链接拦截器。
 * 第一个拦截器将是最外层的，而最后一个拦截程序将是真正调用的最内层包装器。
 * 通过此方法添加的所有流拦截器将被链接。
 * 参数 interceptors - Array<StreamServerInterceptor> 数据类型
 * 返回值 ServerOption - 接口类型
 */
public func chainStreamInterceptor(interceptors: Array<StreamServerInterceptor>): ServerOption

/**
 * 返回一个 ServerOption，该选项指定一元RPC的链接拦截器。
 * 第一个拦截器将是最外层的，而最后一个拦截程序将是真正调用的最内层包装器。
 * 通过此方法添加的所有一元拦截器都将被链接。
 * 参数 interceptors - Array<UnaryServerInterceptor> 数据类型
 * 返回值 ServerOption - 接口类型
 */
public func chainUnaryInterceptor(interceptors: Array<UnaryServerInterceptor>): ServerOption

/**
 * 返回一个 ServerOption，该选项设置所有新连接的连接建立超时（包括HTTP/2握手）。
 * 如果未设置，默认值为120秒。
 * 零值或负值将导致立即超时。
 * 实验的
 * 注意：此API是实验性的，可能会在以后的版本中更改或删除。
 * 参数 d - Duration 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func connectionTimeout(d: Duration): ServerOption

/**
 * 返回一个 ServerOption，该选项设置用于消息封送和解封送的编解码器。
 * 这将覆盖通过RegisterCodec注册的编解码器的任何内容子类型查找。
 * 不推荐使用：使用encoding.RegisterCodec注册编解码器。服务器将根据传入请求的头自动使用注册的编解码器。
 * 参数 codec - BaseCodec 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func customCodec(codec: BaseCodec): ServerOption

/**
 * 返回一个 ServerOption，用于设置流的动态头表的大小。
 * 参数 num - UInt32 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func headerTableSize(num: UInt32): ServerOption

/**
 * 返回一个 ServerOption，该选项设置要创建的所有服务器传输的点击句柄。只能安装一个。
 * 参数 handle - ServerInHandle 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func inTapHandle(handle: ServerInHandle): ServerOption

/**
 * 返回设置连接窗口大小的服务器选项。
 * 窗口大小的下限为64K，任何小于该值的值都将被忽略。
 * 参数 s - Int32 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func initialConnWindowSize(s: Int32): ServerOption

/**
 * 返回为服务器设置keepalive强制策略的ServerOption。
 * 参数 ep - EnforcementPolicy 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func keepaliveEnforcementPolicy(ep: EnforcementPolicy): ServerOption

/**
 * 返回一个 ServerOption，用于设置服务器的keepalive和max age参数。
 * 参数 kp - ServerParameters 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func keepaliveParams(kp: ServerParameters): ServerOption

/**
 * 返回一个 ServerOption，该选项将对每个ServerTransport的并发流数量施加限制。
 * 参数 mc - UInt32 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func maxConcurrentStreams(mc: UInt32): ServerOption

/**
 * 返回一个 ServerOption，该选项设置服务器准备接受的头列表的最大（未压缩）大小。
 * 参数 num - UInt32 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func maxHeaderListSize(num: UInt32): ServerOption

/**
 * MaxMsgSize返回一个 ServerOption，以设置服务器可以接收的最大消息大小（以字节为单位）。
 * 如果未设置，gRPC将使用默认限制。
 * 不推荐使用：改用MaxRecvMsgSize。
 * 参数 m - Int64 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func maxMsgSize(m: Int64): ServerOption

/**
 * 返回服务器选项，以设置服务器可以接收的最大消息大小（以字节为单位）。
 * 如果未设置，gRPC将使用默认的4MB。
 * 参数 mr - Int64 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func maxRecvMsgSize(mr: Int64): ServerOption

/**
 * 返回服务器选项，以设置服务器可以发送的最大消息大小（以字节为单位）。
 * 如果未设置，gRPC将使用默认的“math.MaxInt32”。
 * 参数 num - Int64 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func maxSendMsgSize(num: Int64): ServerOption

/**
 * 返回一个 ServerOption，该选项设置应用于处理传入流的工作进程的数量。
 * 将其设置为零（默认值）将禁用worker并为每个流生成一个新的线程。
 * 注意：此API是实验性的，可能会在以后的版本中更改或删除。
 * 参数 num - UInt32 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func numStreamWorkers(num: UInt32): ServerOption

/**
 * 允许您设置读取缓冲区的大小，这决定了一次读取系统调用最多可以读取多少数据。
 * 此缓冲区的默认值为32KB。
 * Zero将禁用连接的读取缓冲区，以便数据成帧器可以直接访问底层conn。
 * 参数 s - Int64 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func readBufferSize(s: Int64): ServerOption

/**
 * 返回一个 ServerOption，用于设置服务器的统计处理程序。
 * 参数 h - GrpcHandler 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func statsHandler(h: GrpcHandler): ServerOption

/**
 * 返回一个 ServerOption，该选项设置服务器的StreamServerInterceptor。
 * 只能安装一个流拦截器。
 * 参数 si - StreamServerInterceptor 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func streamInterceptor(si: StreamServerInterceptor): ServerOption

/**
 * 返回设置服务器的UnaryServerInterceptor的ServerOption。
 * 只能安装一个一元拦截器。
 * 多个拦截器的构造（例如链接）可以在调用方实现。
 * 参数 usi - UnaryServerInterceptor 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func unaryInterceptor(usi: UnaryServerInterceptor): ServerOption

/**
 * 返回允许添加自定义未知服务处理程序的ServerOption。
 * 提供的方法是一个bidi-streaming RPC服务处理程序，每当收到未注册服务或方法的请求时，将调用该处理程序，而不是返回“未实现”的gRPC错误。
 * 处理函数和流拦截器（如果设置）可以完全访问ServerStream，包括其上下文。
 * 参数 streamHandler - StreamHandler 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func unknownServiceHandler(streamHandler: StreamHandler): ServerOption

/**
 * 确定在对线路进行写入之前可以批处理多少数据。
 * 该缓冲区的相应内存分配将是保持系统调用低的大小的两倍。
 * 此缓冲区的默认值为32KB。
 * 零将禁用写入缓冲区，以便每次写入都在基础连接上。
 * 注意：发送呼叫可能不会直接转换为写入。
 * 参数 s - Int64 数据类型
 * 返回值 ServerOption - 接口类型，返回为服务器连接设置凭据的ServerOption。
 */
public func writeBufferSize(s: Int64): ServerOption
```

#### class ServerStream
实现服务器端流。

```cangjie
public class ServerStream <: Stream {
    
    /**
     * 根据可选择参数，创建 ServerStream 实例。
     * 参数 stss - ServerTransport 数据结构，是所有gRPC服务器端传输实现的通用父类
     * 参数 stream - Stream 数据结构，表示传输层中的RPC
     * 参数 parser - Parser 数据结构，解析器从底层读取器读取完整的gRPC消息
     * 参数 codec - BaseCodec 数据类型，包含编解码器和编码的功能
     * 参数 maxReceiveMessageSize - Int64 数据类型
     * 参数 maxSendMessageSize - Int64 数据结构
     * 参数 statsHandler - Array<GrpcHandler> 数据类型，GrpcHandler 定义相关统计处理的接口
     */
    public init(stss!: ServerTransport,
            stream!: Stream = Stream(isNone: true),
            parser!: Parser = Parser(),
            codec!: BaseCodec = BaseCodec(),
            maxReceiveMessageSize!: Int64 = 0,
            maxSendMessageSize!: Int64 = 0,
            statsHandler!: Array<GrpcHandler> = Array<GrpcHandler>()
            )
    
    /**
     * 设置头元数据。它可以被多次调用。
     * 多次调用时，将合并所有提供的元数据。
     * 发生以下情况之一时，将发送所有元数据：
     *  -调用 sendHeader()；
     *  -发出第一个响应；
     *  -发送RPC状态（错误或成功）。
     * 参数 md - HashMap 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public override func setHeader(md: HashMap<String, Array<String>>): GrpcError
    
    /**
     * 发送标头元数据。将发送由 setHeader() 设置的提供的md和标头。
     * 如果多次调用，则会失败。
     * 参数 md - HashMap 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public override func sendHeader(md: HashMap<String, Array<String>>): GrpcError
    
    /**
     * 设置将随RPC状态一起发送的尾部元数据。多次调用时，将合并所有提供的元数据。
     * 参数 md - HashMap 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public override func setTrailer(md: HashMap<String, Array<String>>): GrpcError
    
    /**
     * 发送消息。出现错误时，sendMsg中止流，并直接返回错误。
     *  sendMsg阻止，直到：
     *    -有足够的流量控制来安排m的运输，或
     *    -流完成，或
     *    -溪流中断。
     * 不会等到客户端收到消息。过早关闭流可能会导致消息丢失。
     * 让一个goroutine同时调用SendMsg和另一个goroutine同时在同一个流上调用RecVMS是安全的，
     * 但在不同的goroutines中在同一流上调用SendMsg是不安全的。
     * 参数 m - Message 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     * 注意：参数 m 在当前版本支持的参数类型为MessageLite，后续版本更迭会修改参数类型为Any
     */
    public func sendMsg(m: Message): GrpcError
    
    /**
     * 阻塞，直到它接收到m中的消息或流完成。
     * 当客户端执行CloseSend时，它返回io EOF。对于任何非EOF错误，流将被中止，错误包含RPC状态。
     * 让一个goroutine同时调用SendMsg和另一个goroutine同时在同一个流上调用RecVMs是安全的，
     * 但在不同的goroutines中在同一流上调用RECVMs是不安全的。
     * 参数 m - Message 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     * 注意：参数 m 在当前版本支持的参数类型为MessageLite，后续版本更迭会修改参数类型为Any
     */
    public func recvMsg(m: Message): GrpcError
}
```

#### class MethodDesc
表示RPC服务的方法规范。

```cangjie
public class MethodDesc {
    /**
     * 根据可选择参数，创建 MethodDesc 实例。
     * 参数 methodName - String 数据结构
     * 参数 handler - 函数 数据结构
     */
    public init(methodName!: String = "",
                handler!: (Message, (Message) -> GrpcError, ?UnaryServerInterceptor) -> (Message, GrpcError))
}
```

#### class ServiceDesc
表示RPC服务的规范.

```cangjie
public class ServiceDesc {
    /**
     * 根据可选择参数，创建 ServiceDesc 实例。
     * 参数 serviceName - String 数据结构
     * 参数 handlerType - Any 数据结构, 指向服务接口的指针。
     * 参数 methods - Array<MethodDesc> 数据结构
     * 参数 streams - Array<StreamDesc> 数据结构
     * 参数 metadata - Any 数据结构
     */
    public init(serviceName!: String = "",
                handlerType!: Any,
                methods!: Array<MethodDesc> = Array<MethodDesc>(),
                streams!: Array<StreamDesc> = Array<StreamDesc>(),
                metadata!: Any)
}
```

#### class MethodInfo
包含有关RPC的信息，包括其方法名称和类型。

```cangjie
public class MethodInfo {
    /**
     * 根据可选择参数，创建 MethodInfo 实例。
     * 参数 name - String 数据结构，该名称只是方法名，不包含服务名或包名。
     * 参数 isClientStream - Bool 数据结构，指示RPC是否是客户端流式RPC。
     * 参数 isServerStream - Bool 数据结构，指示RPC是否是服务器流式RPC。
     */
    public init(name!: String = "", isClientStream!: Bool = false, isServerStream!: Bool = false)
}
```

#### class ServiceInfo
包含服务的一元RPC方法信息、流式RPC方法信息和元数据。

```cangjie
public class ServiceInfo {
    /**
     * 根据可选择参数，创建 ServiceInfo 实例。
     * 参数 methods - Array<MethodInfo> 数据结构
     * 参数 metadata - Any 数据结构，元数据是注册服务时在ServiceDesc中指定的元数据。
     */
    public init(methods!: Array<MethodInfo> = Array<MethodInfo>(), metadata!: Any)
}
```

#### class StreamHandler
定义gRPC服务器调用的处理程序，以完成流式RPC的执行。

```cangjie
public class StreamHandler {    
    /**
     * 根据参数函数f，创建 StreamHandler 实例。
     * 参数 f - 函数类型
     */
    public init(f: (Any, ServerStream) -> GrpcError)
}
```

#### class UnaryHandler
定义由UnaryServerInterceptor调用的处理程序，以完成一元RPC的正常执行。  
如果一个UnaryHandler返回一个错误，它应该由状态包生成，或者是上下文错误之一。  
否则，gRPC将使用Unknown作为状态代码，使用Error()作为RPC的状态消息。

```cangjie
public class UnaryHandler { 
    /**
     * 根据参数函数f，创建 StreamServerInterceptor 实例。
     * 参数 f - 函数类型
     */
    public init(f: (Any) -> (Any, GrpcError))
}
```

#### class StreamServerInterceptor
提供了一个钩子来拦截服务器上流式RPC的执行。
info包含拦截器可以操作的RPC的所有信息。处理程序是服务方法实现。拦截器负责调用处理程序来完成RPC。

```cangjie
public class StreamServerInterceptor { 
    /**
     * 根据参数函数f，创建 StreamServerInterceptor 实例。
     * 参数 f - 函数类型
     */
    public init(f: (Any, ServerStream, StreamServerInfo, StreamHandler) -> GrpcError)
}
```

#### class UnaryServerInterceptor
提供了一个钩子来拦截服务器上一元RPC的执行。info包含拦截器可以操作的这个RPC的所有信息。  
handler是服务方法实现的包装器。拦截器负责调用处理程序来完成RPC。

```cangjie
public class UnaryServerInterceptor {   
    /**
     * 根据参数函数f，创建 StreamServerInterceptor 实例。
     * 参数 f - 函数类型
     */
    public init(f: (Any, UnaryServerInfo, UnaryHandler) -> (Message, GrpcError))
}
```

#### class StreamServerInfo
包含关于服务器端流式RPC的各种信息。拦截器可以改变所有每rpc信息。

```cangjie
public class StreamServerInfo {
    /**
     * 根据可选择参数，创建 StreamServerInfo 实例。
     * 参数 fullMethod - String 数据结构，完整的RPC方法字符串，即/package.service/method。
     * 参数 isClientStream - Bool 数据结构，指示RPC是否是客户端流RPC。
     * 参数 isServerStream - Bool 数据结构，指示RPC是否是服务器流RPC。
     */
    public init(fullMethod!: String = "",
         isClientStream!: Bool = false,
         isServerStream!: Bool = false)
}
```

#### class UnaryServerInfo
包含关于服务器端一元RPC的各种信息。拦截器可以改变所有每rpc信息。

```cangjie
public class UnaryServerInfo {
    /**
     * 根据可选择参数，创建 UnaryServerInfo 实例。
     * 参数 server - Any 数据结构，服务器是用户提供的服务实现。这是只读的。
     * 参数 fullMethod - String 数据结构，是完整的RPC方法字符串，即/package.service/method。
     */
    public init(server!: Any, fullMethod!: String = "")
}
```

#### class ServerConfig
包含建立服务器传输的所有配置。

```cangjie
public class ServerConfig {
    /**
     * 根据可选择参数，创建 ServerConfig 实例。
     * 参数 maxStreams - UInt32 数据结构
     * 参数 connectionTimeout - Duration 数据结构
     * 参数 inTapHandle - ServerInHandle 数据结构
     * 参数 statsHandlers - ArrayList<GrpcHandler> 数据结构
     * 参数 keepaliveParams - ServerParameters 数据结构
     * 参数 keepalivePolicy - EnforcementPolicy 数据结构
     * 参数 initialWindowSize - Int32 数据结构
     * 参数 initialConnWindowSize - Int32 数据结构
     * 参数 writeBufferSize - Int64 数据结构
     * 参数 readBufferSize - Int64 数据结构
     * 参数 maxHeaderListSize - UInt32 数据结构
     * 参数 headerTableSize - UInt32 数据结构
     */
    public init(
                maxStreams            !: UInt32 = 0,
                connectionTimeout     !: Duration = Duration.second(),
                inTapHandle           !: ServerInHandle = ServerInHandle(),
                statsHandlers         !: ArrayList<GrpcHandler> = ArrayList<GrpcHandler>(),
                keepaliveParams       !: ServerParameters = ServerParameters(),
                keepalivePolicy       !: EnforcementPolicy = EnforcementPolicy(),
                initialWindowSize     !: Int32 = 0,
                initialConnWindowSize !: Int32 = 0,
                writeBufferSize       !: Int64 = 0,
                readBufferSize        !: Int64 = 0,
                maxHeaderListSize     !: UInt32 = 0,
                headerTableSize       !: UInt32 = 0)
}
```

#### class Stream
表示传输层中的RPC。

```cangjie
public open class Stream <: InputStream {
    
    /**
     * 根据可选择参数，创建 Stream 实例。
     * 参数 id - UInt32 数据结构，流 id
     * 参数 method - String 数据结构，mothod 名称
     * 参数 recvCompress - String 数据结构，接收压缩名称
     * 参数 st - Option<ServerTransport> 数据结构，ServerTransport 是所有 gRPC 服务器端传输实现的通用父类
     * 参数 fc - InFlow 数据结构，流入处理入站流量控制
     * 参数 contentSubtype - String 数据结构，是请求的内容子类型
     * 参数 isNone - Bool 数据结构，创建的实例是否是空结构
     */
    public init(id!: UInt32 = 0,
                method! : String = "",
                recvCompress! : String = "",
                st!: ?ServerTransport = None,
                fc!: InFlow = NULL_INFLOW,
                contentSubtype!: String = "",
                isNone!: Bool = false)

    /**
     * 从输入流中读取数据放到 data 中
     * 参数 data - 读取数据存放的缓冲区，若 buffer 为空则抛出异常
     * 返回值 Int64 - 读取成功，返回读取字节数
     * 若流被关闭或者没有数据可读，则返回 0
     * 读取失败，则抛出异常
     */
    public func read(data: Array<Byte>): Int64
    
    /**
     * 返回应用于入站消息的压缩算法。如果未应用压缩，则为空字符串。
     * 返回值 String - 返回应用于入站消息的压缩算法名称
     */
    public func getRecvCompress(): String
    
    /**
     * 拖车返回缓存的拖车metedata。请注意，如果在完成整个流之后不调用它，它可能只返回空的MD.客户端。
     * 只有在流结束后，即读或写返回io.EOF，它才能安全地读取。
     * 返回值 HashMap<String, Array<String>> - 返回拖车返回缓存的拖车metedata
     */
    public func getTrailer(): HashMap<String, Array<String>>
    
    /**
     * 返回请求的内容子类型。
     * 返回值 String - 返回请求的内容子类型名称
     */
    public func getContentSubtype(): String
    
    /**
     * 返回流的方法。
     * 返回值 String - 返回流的方法名称
     */
    public func getMethod(): String
    
    /**
     * 状态返回从服务器接收的状态。只有在流结束后，也就是在完成关闭后，才能安全读取状态。
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public func getStatus(): GrpcError
    
    /**
     * 设置头元数据。这可以被称为多次。仅限服务器端。这不应与其他数据写入并行调用。
     * 参数 md - HashMap<String, Array<String>>，头部的键值对数据
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public open func setHeader(md: HashMap<String, Array<String>>): GrpcError
    
    /**
     * 发送给定的头元数据。给定元数据与之前调用SetHeader设置的任何元数据组合，然后写入传输流。
     * 参数 md - HashMap<String, Array<String>>，头部的键值对数据
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public open func sendHeader(md: HashMap<String, Array<String>>): GrpcError
    
    /**
     * 设置服务器将随RPC状态一起发送的尾部元数据。这可以调用多次。仅服务器端。
     * 这不应与其他数据写入并行调用。
     * 参数 md - HashMap<String, Array<String>>，头部的键值对数据
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public open func setTrailer(md: HashMap<String, Array<String>>): GrpcError
}
```

#### class ServerTransport
提供了一个钩子来拦截服务器上一元RPC的执行。info包含拦截器可以操作的这个RPC的所有信息。  
handler是服务方法实现的包装器。拦截器负责调用处理程序来完成RPC。

```cangjie
public abstract class ServerTransport <: Hashable & Equatable<ServerTransport> {
    
    /**
     * 使用给定的处理程序接收传入流。
     * 参数 handle - 函数类型
     * 参数 traceCtx - 函数类型
     */
    public func handleStreams(handle: (Stream) -> Unit, traceCtx: (String) -> Unit): Unit

    /**
     * 发送给定流的头元数据。不能对所有流调用WriteHeader。
     * 参数 s - Stream 数据结构
     * 参数 md - HashMap 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public func writeHeader(s: Stream, md: HashMap<String, Array<String>>): GrpcError

    /**
     * 发送给定流的数据。不能对所有流调用Write。
     * 参数 s - Stream 数据结构
     * 参数 hdr - Array<UInt8> 数据结构
     * 参数 data - Array<UInt8> 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public func write(s: Stream, hdr: Array<UInt8>, data: Array<UInt8>): GrpcError

    /**
     * 将流的状态发送给客户端。WriteStatus是对流进行的最后一个调用，并且总是发生。
     * 参数 s - Stream 数据结构
     * 参数 st - GrpcError 数据结构
     * 返回值 GrpcError - GrpcError 数据结构，封装错误信息的类
     */
    public func writeStatus(s: Stream, st: GrpcError): GrpcError

    /**
     * 关闭会撕裂传输线。一旦调用，就不应再访问传输。所有挂起的流及其处理程序将异步终止。
     */
    public func close(): Unit

    /**
     * 返回远程网络地址。
     * 返回值 SocketAddress - SocketAddress 数据结构，返回远程网络地址。
     */
    public func getRemoteAddr(): SocketAddress

    /**
     * 通知客户端此ServerTransport停止接受新的RPC。
     */
    public func drain(): Unit

    /**
     * 增加通过此传输发送的消息数。
     */
    public func incrMsgSent(): Unit

    /**
     * 增加通过此传输接收的消息数。
     */
    public func incrMsgRecv(): Unit
}
```

#### interface GrpcHandler
定义相关统计处理的接口（例如，RPC、连接）。

```cangjie
public interface GrpcHandler {

    /**
     * 传入参数info，执行函数。
     * 参数 info - RPCTagInfo 数据结构，定义 RPC 上下文标记器所需的相关信息
     */
    func tagRPC(info: RPCTagInfo): Unit

    /**
     * 处理RPC统计数据。
     * 参数 stats - RPCStats 数据结构，包含关于RPC的统计信息。
     */
    func handleRPC(stats: RPCStats): Unit

    /**
     * 可以将一些信息附加到给定的上下文中。返回的上下文将用于统计处理。对于conn stats处理，HandleConn中用于此连接的上下文将从返回的上下文派生。
     * 对于RPC状态处理，
     *   -在服务器端，HandleRPC中用于此连接上所有RPC的上下文将从返回的上下文派生。
     *   -在客户端，上下文不是从返回的上下文派生的。
     * 参数 ctInfo - ConnTagInfo 数据结构
     */
    func tagConn(ctInfo: ConnTagInfo): Unit

    /**
     * 处理Conn统计数据。
     * 参数 cs - ConnStats 数据结构
     */
    func handleConn(cs: ConnStats): Unit
}
```

#### class ConnTagInfo
定义连接上下文标记器所需的相关信息。

```cangjie
public class ConnTagInfo {
    /**
     * 根据可选择参数，创建 ConnTagInfo 实例。
     * 参数 remoteAddr - SocketAddress 数据类型，是相应连接的远程地址
     * 参数 localAddr - SocketAddress 数据类型，是相应连接的本地地址。
     */
    public init(remoteAddr!: SocketAddress = DEFAULT_SADDRESS, localAddr!: SocketAddress = DEFAULT_SADDRESS)

}
```

#### class RPCTagInfo
定义RPC上下文标记器所需的相关信息。

```cangjie
public class RPCTagInfo {
    /**
     * 根据可选择参数，创建 RPCTagInfo 实例。
     * 参数 fullMethodName - String 数据类型，package.service/method格式的RPC方法。
     * 参数 failFast - Bool 数据类型，此RPC是否为FailFat。此字段仅在客户端有效，在服务器端始终为false。
     */
    public init (fullMethodName!: String = "", failFast!: Bool = false) {
        this.fullMethodName = fullMethodName
        this.failFast = failFast
    }

}
```

#### interface RPCStats
包含关于RPC的统计信息。

```cangjie
public interface RPCStats {
    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    func isClient(): Bool
}
```

#### class Begin
接口RPCStats的实现类，包含RPC尝试开始时的统计信息。FailFast仅在从客户端开始时有效。

```cangjie
public class Begin <: RPCStats {
    /**
     * 根据可选择参数，创建 Begin 实例。
     * 参数 client - 如果来自客户端，则为 true。
     * 参数 beginTime - Time 数据结构， 是RPC尝试开始的时间
     * 参数 failFast - Bool 数据类型，指示此RPC是否为FailFast
     * 参数 isClientStream - Bool 数据类型，指示RPC是否是客户端流式RPC
     * 参数 isServerStream - Bool 数据类型，指示RPC是否为服务器流式RPC
     * 参数 isTransparentRetryAttempt - Bool 数据结构， 指示此尝试是否是由于透明地重试以前的尝试而启动的
     */
    public init(client!: Bool = false,
                beginTime!: Time = Time.now(),
                failFast!: Bool = false,
                isClientStream!: Bool = false,
                isServerStream!: Bool = false,
                isTransparentRetryAttempt!: Bool = false)

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class InPayload
接口RPCStats的实现类，包含传入有效负载的信息。

```cangjie
public class InPayload <: RPCStats {
    /**
     * 根据可选择参数，创建 InPayload 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     * 参数 payload - Any 数据结构，原始类型的有效载荷
     * 参数 data - Array<UInt8> 数据类型，序列化消息负载
     * 参数 length - Int64 数据类型，未压缩数据的长度
     * 参数 wireLength - Int64 数据类型，有线数据的长度（压缩、签名、加密）
     * 参数 recvTime - Time 数据结构，收到有效载荷的时间
     */
    public init(client!: Bool = false,
                payload!: Any,
                data!: Array<UInt8> = EMPTY_ARRAY_UINT8,
                length!: Int64 = 0,
                wireLength!: Int64 = 0,
                recvTime!: Time = Time.now())

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class InHeader
接口RPCStats的实现类，包含收到报头时的统计信息。

```cangjie
public class InHeader <: RPCStats {
    /**
     * 根据可选择参数，创建 InHeader 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     * 参数 wireLength - Int64 数据类型，头部的有线长度
     * 参数 compression - String 数据类型，用于RPC的压缩算法
     * 参数 header - HashMap<String, Array<String>> 数据类型，包含接收到的标头元数据
     * 参数 fullMethod - String 数据结构，是完整的RPC方法字符串，即/package.service/method
     * 参数 remoteAddr - SocketAddress 数据类型，相应连接的远程地址
     * 参数 localAddr - SocketAddress 数据结构，相应连接的本地地址
     */
    public init(client!: Bool = false, wireLength!: Int64 = 0, compression!: String = "",
            header!: HashMap<String, Array<String>> = HashMap<String, Array<String>>(),
            fullMethod!: String = "", remoteAddr!: SocketAddress = EMPTY_ADDR,
            localAddr!: SocketAddress = EMPTY_ADDR)

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class InTrailer
接口RPCStats的实现类，包含接收到拖车时的统计信息。

```cangjie
public class InTrailer <: RPCStats {
    /**
     * 根据可选择参数，创建 InTrailer 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     * 参数 wireLength - Int64 数据类型，trailer 的长度。
     * 参数 trailer - HashMap<String, Array<String>> 数据结构，拖车包含从服务器接收的拖车元数据。仅当此InTrailer来自客户端时，此字段才有效。
     */
    public init(client!: Bool = false, wireLength!: Int64 = 0,
            trailer!: HashMap<String, Array<String>> = HashMap<String, Array<String>>())

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class OutPayload
接口RPCStats的实现类，包含传出有效负载的信息。

```cangjie
public class OutPayload <: RPCStats {
    /**
     * 根据可选择参数，创建 OutPayload 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     * 参数 payload - Any 数据结构，原始类型的有效载荷
     * 参数 data - Array<UInt8> 数据类型，序列化消息负载
     * 参数 length - Int64 数据类型，未压缩数据的长度
     * 参数 wireLength - Int64 数据类型，有线数据的长度（压缩、签名、加密）
     * 参数 sentTime - Time 数据结构，发送有效载荷的时间
     */
    public init(client!: Bool = false,
                payload!: Any,
                data!: Array<UInt8> = EMPTY_ARRAY_UINT8,
                length!: Int64 = 0,
                wireLength!: Int64 = 0,
                sentTime!: Time = Time.now()
    )
    
    /**
     * 根据可选择参数，创建 OutPayload 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     * 参数 msg - Any 数据结构，原始类型的有效载荷
     * 参数 data - Array<UInt8> 数据类型，序列化消息负载；data.size 为 length
     * 参数 payload - Array<UInt8> 数据类型，payload + HEADER_LEN(5)  = wireLength
     * 参数 t - Time 数据结构，发送有效载荷的时间
     */
    public static func getOutPayload(client: Bool, msg: Any, data: Array<UInt8>, payload: Array<UInt8>, t: Time): OutPayload

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class OutHeader
接口RPCStats的实现类，发送标头时包含统计信息。

```cangjie
public class OutHeader <: RPCStats { 
    /**
     * 根据可选择参数，创建 OutHeader 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     * 参数 compression - String 数据类型，用于RPC的压缩算法
     * 参数 header - HashMap<String, Array<String>> 数据类型，包含发送的标头元数据
     * 参数 fullMethod - String 数据结构，是完整的RPC方法字符串，即/package.service/method
     * 参数 remoteAddr - SocketAddress 数据类型，相应连接的远程地址
     * 参数 localAddr - SocketAddress 数据结构，相应连接的本地地址
     */
    public init(client!: Bool = false, compression!: String = "",
            header!: HashMap<String, Array<String>> = HashMap<String, Array<String>>(),
            fullMethod!: String = "", remoteAddr!: SocketAddress = EMPTY_ADDR,
            localAddr!: SocketAddress = EMPTY_ADDR)

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class OutTrailer
接口RPCStats的实现类，包含发送预告片时的统计信息。

```cangjie
public class OutTrailer <: RPCStats {
    /**
     * 根据可选择参数，创建 OutTrailer 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     * 参数 wireLength - Int64 数据类型，trailer 的长度。
     * 参数 trailer - HashMap<String, Array<String>> 数据结构，拖车包含发送给客户端的拖车元数据。此字段仅在该输出尾文件来自服务器端时有效。
     */
    public init(client!: Bool = false, wireLength!: Int64 = 0,
            trailer!: HashMap<String, Array<String>> = HashMap<String, Array<String>>())

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class End
接口RPCStats的实现类，包含RPC结束时的统计信息。

```cangjie
public class End <: RPCStats {
    /**
     * 根据可选择参数，创建 End 实例。
     * 参数 client - 如果来自客户端，则为 true。
     * 参数 beginTime - Time 数据结构，RPC 尝试开始的时间
     * 参数 endTime - Time 数据结构，RPC 结束的时间。
     * 参数 trailer - HashMap<String, Array<String>> 数据类型
     * 参数 error - GrpcError 数据类型
     */
    public init(client!: Bool = false,
                beginTime!: Time = Time.now(),
                endTime!: Time = Time.now(),
                trailer!: HashMap<String, Array<String>> = HashMap<String, Array<String>>(),
                error!: GrpcError = NULL_ERR)

    /**
     * 如果此RPCStats来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果此RPCStats来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### interface ConnStats
包含关于连接的统计信息。

```cangjie
public interface ConnStats {
    /**
     * 如果来自客户端，则返回true。
     * 返回值 Bool - Bool 数据结构，如果来自客户端，则返回true。
     */
    func isClient(): Bool
}
```

#### class ConnBegin
接口ConnStats的实现类，包含连接建立时的状态。

```cangjie
public class ConnBegin <: ConnStats {
    /**
     * 根据可选择参数，创建 ConnBegin 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     */
    public init(client!: Bool = false)

    /**
     * 是否来自客户端.
     * 返回值 Bool - Bool 数据结构，如果来自客户端，则返回true
     */
    public func isClient(): Bool
}
```

#### class ConnEnd
接口ConnStats的实现类，包含连接结束时的状态。

```cangjie
public class ConnEnd <: ConnStats {
    /**
     * 根据可选择参数，创建 ConnEnd 实例。
     * 参数 client - 如果来自客户端，则客户端为真
     */
    public init(client!: Bool = false)

    /**
     * 是否来自客户端.
     * 返回值 Bool - Bool 数据结构，如果来自客户端，则返回 true
     */
    public func isClient(): Bool
}
```

#### class ServerParameters
用于在服务器端设置keepalive和max-age参数。

```cangjie
public class ServerParameters {
    /**
     * 根据可选择参数，创建 ServerParameters 实例。
     * 参数 maxConnectionIdle - Duration 数据结构，空闲持续时间是从最近一次未完成的RPC数变为零或连接建立起定义的。当前默认值为无穷大。
     * 参数 maxConnectionAge - Duration 数据结构，连接在通过发送GoAway关闭之前可能存在的最长时间。
     * 参数 maxConnectionAgeGrace - Duration 数据结构，是MaxConnectionAge之后的一个加法时段，在此时段之后，连接将被强制关闭。
     * 参数 time - Duration 数据结构，经过一段时间后，如果服务器没有看到任何活动，它将ping客户端以查看传输是否仍然有效。如果设置为低于1s，则将使用最小值1s。当前默认值为2小时。
     * 参数 timeout - Duration 数据结构，在ping以进行keepalive检查之后，服务器将等待一段时间的超时，如果在此之后没有看到任何活动，则连接将关闭。当前默认值为20秒。
     */
    public init(maxConnectionIdle!: Duration = EMPTY_TIME,
                maxConnectionAge!: Duration = EMPTY_TIME,
                maxConnectionAgeGrace!: Duration = EMPTY_TIME,
                time!: Duration = Duration.hour(2),
                timeout!: Duration = Duration.second(20))
}
```

#### class EnforcementPolicy
用于在服务器端设置keepalive强制策略。服务器将关闭与违反此策略的客户端的连接。

```cangjie
public class EnforcementPolicy {
    /**
     * 根据可选择参数，创建 EnforcementPolicy 实例。
     * 参数 minTime - Duration 数据结构，是客户端在发送保活 ping 之前应等待的最短时间。当前默认值为5分钟
     * 参数 permitWithoutStream - Bool 数据类型，如果为true，则即使在没有活动流（RPC）的情况下，服务器也允许保持活动ping。
     *                           如果为false，并且客户端在没有活动流时发送ping，服务器将发送GOAWAY并关闭连接。
     *                           默认为false。
     */
    public init(minTime!: Duration = Duration.minute(5), permitWithoutStream!: Bool = false)
}
```

#### class Peer
对等体包含RPC对等体的信息，如地址和身份验证信息。

```cangjie
public class Peer {
    /**
     * 根据可选择参数，创建 Peer 实例。
     * 参数 addr - SocketAddress 数据结构，对等地址
     * 参数 authInfo - Option<AuthInfo> 数据类型，传输的身份验证信息。如果没有使用传输安全，则为None。
     */
    public init(addr!: SocketAddress = EMPTY_ADDR, authInfo!: Option<AuthInfo> = None)
}
```


#### class Cilentconn

主要用于grpc提供客户端端相应接口，客户端根据protobuf所定义的参数创建参数数据调用rpc方法，并接收返回值

```cangjie
public interface ClientConnInterface {
    //为了调用服务方法，首先创建一个 gRPC channel 和服务器交互
    public func Dial(target:String)
    
    //执行响应,创建ClientStream用于收发消息
    public func Invoke(method string, con: Cilentconn): Any
}

public class Cilentconn <: ClientConnInterface {
    //客户端等待，直到等到连接状态改变或超时
    public func WaitForStateChange(sourceState:connectivity.State)
    
    //返回当前客户端状态，枚举类型
    public func GetState(): connectivity.State
    
    //解析服务器地址
    private func waitForResolvedAddrs(): Bool
    
    //为经常失败的RPC连接配置负载均衡，
    private func applyFailingLB(sc: serviceconfig.ParseResult)
    
    //为Cilentconn创建一条新的连接
    public func newAddrConn（addr: resolver.Address: addrConn
    
    //移除一条连接
    public func removeAddrConn(ac addrConn): Bool
    
}
```

```cangjie
type Stream interface {
     //收发消息调用http2 transport
    public func sendMsg(m: object): Bool
    //收取消息
    public func RecvMsg(m: object): Bool
}

//收发信息流
public class ClientStream <: Stream {
    //返回从服务器接收到的标头元数据
    public func Header(): metadata.MD
    
    //从服务器返回响应标头数据
    public func Trailer(): metadata.MD
    
    //收发消息调用http2 transport
    public func sendMsg(m: Any): Bool
    
    //http2关闭发送流
    public func CloseSend()
    
    //收取消息
    public func RecvMsg(m: Any): Bool
    
    //关闭流，以 EOF 结束流表示关闭成功
    public func finish()
    
    //重试当前连接
    public func withRetry()
    
    //根据指定的addr和http transport创建newClientStream
    public func newNonRetryClientStream(desc :StreamDesc, method: string, t transport.ClientTransport, ac: addrConn)
}
```

#### class ClientTransport

该类用于数据的网络传输

```cangjie
public class ClientTransport{
    //向指定流发送标头元数据
    public func WriteHeader(s :Stream, md :metadata.MD): Bool

    //向指定流发送标头元数据
    public func Write(s :Stream, hdr :Array<UInt8>, data :Array<UInt8>): Bool

    //将流的状态发送到客户端.
    public func WriteStatus(s :Stream, st :status.Status): Bool

    //关闭当前流的传输.
    public func Close()

    //返回服务器的地址s.
    public func RemoteAddr():net.Addr

    //ServerTransport 停止接受新的 RPC请求.
    public func Drain()

    //增加通过此传输发送的消息数.
    public func IncrMsgSent()

    //增加通过此传输接收到的消息数.
    public func IncrMsgRecv()
}
```

### class ClientConn

```
/*
 * 发起一个 grpc 请求, 该 api 为 ClientConn 的核心 api
 * 参数 method - 调用的 grpc 方法名
 * 参数 input - 该方法参数
 * 参数 output - 用于接收该方法返回值
 */
public func invoke(method: String, input: Message, output: Message): Unit

/*
 * 获取当前 clientconn 目标地址
 * 返回值 String - 当前 grpc 的请求目标地址
 */
public func getTarget(): String

/*
 * 关闭当前和服务器的连接
 */
public func close()

/*
 * 获取当前 grpc 请求状态
 * 返回值 ConState - 当前 grpc 请求状态
 */
public func getState(): ConState

/*
 * 添加新的地址目标容器 clientconn 的 conn 属性
 * 参数 String - 要添加目标地址
 */
public func newAddrConn(addr: String): Unit

/*
 * 判断当前状态是否等于 sourceState
 * 参数 sourceState - 判断的状态
 * 返回值 Bool - 是否相等
 */
public func WaitForStateChange(sourceState: ConState): Bool

/*
 * 获取方法的 MethodConfig 信息
 * 参数 method - 请求方法名
 * 返回值 MethodConfig - 方法配置实例对象
 */
public func GetMethodConfig(method: String): MethodConfig


/*
 * 启用默认 zlib 内置压缩器压缩请求数据
 */
public func useCompressor(): Unit

/*
 * 创建流式客户端
 * 返回值 ClientStream - 返回一个流式客户端
 */
public func newStreamClient(): ClientStream
```

### class ClientStream

```
/*
 * 指定要请求的服务
 * 参数 method - 要请求的服务
 */
public func doStreamRequest(method: String): Unit

/*
 * 发送流数据
 * 参数 message - 要发送的 protobuf 请求对象
 */
public func sendStreamMsg(message: Message): Unit

/*
 * 停止发送并开始接收服务器响应
 * 返回值 ReadStream - 返回一个 ReadStream 用于读取接收到的响应数据
 */
public func closeAndRecv(): ReadStream
```

### class ReadStream

```
/*
 * 从返回流读取 protobuf 对象
 * 参数 output - 用于接收读取到的 protobuf 对象
 * 返回值 Bool - 是否成功读取到，true 为读取成功， false 为未读到数据
 */
public func recv(output: Message): Bool
```

### interface UnaryInvoker

```
/*
 * 拦截一元 inkove 请求
 * 参数 method - 请求的 grpc 方法名
 * 参数 req - 请求的参数
 * 参数 resp - 请求的返回值
 */
func orderUnaryClientInterceptor(method: String, req: Message, resp: Message): Unit
```

### interface StreamClientInterceptor

```
/*
 * 拦截客户端流式请求方法名
 * 参数 method - 请求的 grpc 方法名
 */
func orderStreamClientInterceptor(method: String): Unit

/*
 * 拦截客户端流式请求参数
 * 参数 req - 请求的参数
 */
func orderStreamClientInterceptorReq(req: Message): Unit

/*
 * 拦截客户端流式请求返回值
 * 参数 resp - 请求的返回值
 */
func orderStreamClientInterceptorResp(resp: Message): Unit
```

### func dial

```
/*
 * 和 grpc 服务器创建连接, 该 api 为 dial 的核心 api
 * 参数 target - 服务器地址
 * 返回值 ClientConn - 返回一个 ClientConn 实例对象
 */
public func dial(target: String): ClientConn

/*
 * 带额外连接参数的 dial
 * 参数 target - 服务器地址
 * 参数 opts - 连接参数 DialOption 列表，目前仅支持 withResolvers
 * 返回值 ClientConn - 返回一个 ClientConn 实例对象
 */
public func dial(target: String, opts: ArrayList<DialOption>): ClientConn
```

### func DialOption

```
/*
 * 指定 gRPC 是否因非临时拨号错误而失败
 * 参数 bol - 是否启用
 * 返回值 DialOption - 返回的 DialOption
 */
public func failOnNonTempDialError(bol: Bool): DialOption

/*
 * 指定要用作 authority 伪标头的值，并在身份验证握手中用作服务器名称
 * 参数 str - 设置的值
 * 返回值 DialOption - 返回的 DialOption
 */
public func withAuthority(str: String): DialOption

/*
 * 设置在连接失败后使用提供的回退参数。
 * 参数 b - 设置的 BackoffConfig 参数
 * 返回值 DialOption - 返回的 DialOption
 */
public func withBackoffConfig(b: BackoffConfig): DialOption

/*
 * 设置用于连接的回退策略失败后重试次数
 * 参数 bs - 设置的 Strategy
 * 返回值 DialOption - 返回的 DialOption
 */
public func withBackoff(bs: Strategy): DialOption

/*
 * 设置读取缓冲区的大小
 * 参数 s - 设置的大小
 * 返回值 DialOption - 返回的 DialOption
 */
public func withReadBufferSize(s: Int64): DialOption

/*
 * 设置流上初始窗口大小的值
 * 参数 s - 设置的大小
 * 返回值 DialOption - 返回的 DialOption
 */
public func withInitialWindowSize(s: Int32): DialOption

/*
 * 设置连接上初始窗口大小的值
 * 参数 s - 设置的大小
 * 返回值 DialOption - 返回的 DialOption
 */
public func withInitialConnWindowSize(s: Int32): DialOption

/*
 * 设置客户端可以接收的最大消息大小
 * 参数 s - 设置的大小
 * 返回值 DialOption - 返回的 DialOption
 */
public func withMaxMsgSize(s: Int64): DialOption

/*
 * 设置用于消息封送处理和取消封送处理的编解码器
 * 参数 c - 设置的编解码器
 * 返回值 DialOption - 返回的 DialOption
 */
public func withCodec(c: BaseCodec): DialOption

/*
 * 设置使拨号的呼叫者阻塞，直到基础连接启动。
 * 返回值 DialOption - 返回的 DialOption
 */
public func withBlock(): DialOption

/*
 * 确定在网络上执行写入操作之前可以批量处理的数据量
 * 参数 s - 设置的数据量大小
 * 返回值 DialOption - 返回的 DialOption
 */
public func withWriteBufferSize(s: Int64): DialOption

/*
 * 指定所有 RPC 的用户代理字符串
 * 参数 s - 指定的字符串
 * 返回值 DialOption - 返回的 DialOption
 */
public func withUserAgent(s: String): DialOption

/*
 * 配置客户端的超时等待时间
 * 参数 d - 设置等待时间长度
 * 返回值 DialOption - 返回的 DialOption
 */
public func withTimeout(d: Duration): DialOption

/*
 * 使客户端连接返回一个字符串，其中包含最后一个连接错误
 * 返回值 DialOption - 返回的 DialOption
 */
public func withReturnConnectionError(): DialOption

/*
 * 禁止使用此客户端通信的代理
 * 返回值 DialOption - 返回的 DialOption
 */
public func withNoProxy(): DialOption

/*
 * 指定客户端准备的头列表的最大大小
 * 参数 s - 指定的大小
 * 返回值 DialOption - 返回的 DialOption
 */
public func withMaxHeaderListSize(s: UInt32): DialOption

/*
 * 禁用此客户端通讯子的传输安全性
 * 返回值 DialOption - 返回的 DialOption
 */
public func withInsecure(): DialOption

/*
 * 使 gRPC 忽略解析程序提供的任何服务配置
 * 返回值 DialOption - 返回的 DialOption
 */
public func withDisableServiceConfig(): DialOption

/*
 * 禁用重试
 * 返回值 DialOption - 返回的 DialOption
 */
public func withDisableRetry(): DialOption

/*
 * 禁用此客户端康纳的所有子计算机的 LB 通道运行状况检查
 * 返回值 DialOption - 返回的 DialOption
 */
public func withDisableHealthCheck(): DialOption

/*
 * 配置默认服务配置
 * 参数 s - 配置的字符串
 * 返回值 DialOption - 返回的 DialOption
 */
public func withDefaultServiceConfig(s: String): DialOption

/*
 * 配置解析器
 * 参数 rs - 配置的解析器
 * 返回值 DialOption - 返回的 DialOption
 */
public func withResolvers(rs: resolver.Builder): DialOption

/*
 * 配置一元请求拦截器
 * 参数 unary - 配置的一元拦截器
 * 返回值 DialOption - 返回的 DialOption
 */
public func WithUnaryInterceptor(unary: UnaryInvoker): DialOption

/*
 * 配置流式请求拦截器
 * 参数 unary - 配置的流式拦截器
 * 返回值 DialOption - 返回的 DialOption
 */
public func withStreamInterceptor(unary: StreamClientInterceptor): DialOption
```

### calloption

```
/*
 * 返回一个 CallOption，设置客户端可以接收的最大消息大小
 * 参数 m - 设置的大小
 * 返回值 CallOption - 返回的 CallOption
 */
public func maxCallRecvMsgSize(bytes: Int64): CallOption

/*
 * 返回一个 CallOption，该选项将设置用于调用的所有请求和响应消息的编解码器
 * 参数 m - 设置的 BaseCodec
 * 返回值 CallOption - 返回的 CallOption
 */
public func callCustomCodec(codec: BaseCodec): CallOption

/*
 * 为一个请求设置 content-subtype
 * 参数 m - 设置的 contentSubtype 字符串
 * 返回值 CallOption - 返回的 CallOption
 */
public func CallContentSubtype(contentSubtype: String): CallOption

/*
 * 返回一个 CallOption，该选项将设置用于调用的所有请求和响应消息的编解码器
 * 参数 m - 设置的 BaseCodec
 * 返回值 CallOption - 返回的 CallOption
 */
public func forceCodec(codec: BaseCodec): CallOption

/*
 * 返回一个 CallOption，设置客户端可以发送的最大消息大小
 * 参数 m - 设置的消息大小
 * 返回值 CallOption - 返回的 CallOption
 */
public func maxCallSendMsgSize(bytes: Int64): CallOption

/*
 * 限制用于缓冲此 RPC 的请求进行重试的内存量。
 * 参数 m - 设置的重试的内存量大小
 * 返回值 CallOption - 返回的 CallOption
 */
public func maxRetryRPCBufferSize(bytes: Int64): CallOption

/*
 * 设置发送请求时使用的压缩器
 * 参数 m - 设置使用的压缩器
 * 返回值 CallOption - 返回的 CallOption
 */
public func useCompressor(name: String): CallOption

/*
 * 配置在断开的连接或无法访问的服务器上尝试 RPC 时要执行的操作
 * 参数 m - 设置是否启用
 * 返回值 CallOption - 返回的 CallOption
 */
public func waitForReady(waitForReady: Bool): CallOption

/*
 * 配置 PerRPCCredentials 指示用于呼叫的每个 RPC 凭据
 * 参数 m - 设置的 PerRPCCredentials
 * 返回值 CallOption - 返回的 CallOption
 */
public func perRPCCredentials(creds: PerRPCCredentials): CallOption
```

### enum ConState

```
    |IDLE
    |CONNECTING
    |READY
    |TRANSIENT_FAILURE
    |SHUTDOWN
    |INVALID_STATE

/*
 * 操作符运算 ==
 * 参数 that- 用于比较的其他的 ConState
 * 参数 Bool - 相等返回 true；否则返回 false
 */
public operator func == (that: ConState): Bool

/*
 * 操作符运算 !=
 * 参数 that - 用于比较的其他的 ConState
 * 参数 Bool - 不相等返回 true；否则返回 false
 */
public operator func != (that: ConState): Bool
```

### class MetaData 

```
/*
 * 初始化 MetaData
 * 参数 m - 初始化的 HashMap
 */
public init(m: HashMap<String,ArrayList<String>>)

/*
 * 按 key, value, key2, value2 顺序解析存入 MetaData, 该 api 为 MetaData 的核心 api
 * 参数 kv - 要存入该 MetaData 的 ArrayList
 * 返回值 mataData - 返回当前 MetaData
 */
public func pairs(kv: ArrayList<String>): MetaData

/*
 * 返回该 MetaData 的大小
 * 参数 kv - 要存入该 MetaData 的 ArrayList
 * 返回值 Int64 - MetaData 的大小
 */
public func len(): Int64

/*
 * 复制当前 MetaData
 * 返回值 MetaData - 复制的新 MetaData 对象
 */
public func copy(): MetaData

/*
 * 根据 key 取值
 * 参数 k - key 值
 * 返回值 ArrayList<String> - 返回对应的 value
 */
public func get(k: String): ArrayList<String>

/*
 * 添加键值对
 * 参数 k - key 值
 * 参数 v - value 值
 */
public func set(k: String, v: ArrayList<String>): Unit

/*
 * 添加键值对
 * 参数 k - key 值
 * 参数 v - value 值，已有值则叠加
 */
public func append(k: String, v: ArrayList<String>): Unit 

/*
 * 根据 key 值删除键值对
 * 参数 k - key 值
 */
public func delete(k: String) 
```

```
/*
 * 两个 String 转一个元组
 * 参数 k - key 值
 * 参数 v - value 值
 * 参数 (String, String) - 返回两个 String 的元组
 */
public func DecodeKeyValue(k: String, v: String): (String, String)

/*
 * ArrayList 内多个 MetaData 合并存入一个 MetaData
 * 参数 k - 要合并的 ArrayList<MetaData>
 * 参数 MetaData - 合并完的 MetaData
 */
public func join(mds: ArrayList<MetaData>): MetaData
```

### struct Address
保存地址信息

```cangjie
public struct Address {
    public var addr: String
    public var serverName: String
    public var addrType: UInt8
    public var attributes: attributes.Attributes<Int64,String>

    /**
     * 判断是否和另一个 Address 的所有值是否相等
     * 参数 o - 要对比的另一个 Address 的所有值
     * 返回值 Bool - 值是否相等
     */
    public func equal(o: Address): Bool {
}
```

```cangjie
/**
 * 创建一个 Attributes<K,V>
 * 参数 key - 一个 key 值，该类型必须 实现了Hashable和Equatable<K>
 * 参数 value - 一个 value 值，该类型必须 实现了Equatable<V>
 * 返回值 Bool - 值是否相等
 */
public func new<K,V>(key: K, value: V): Attributes<K,V> where K <: Hashable & Equatable<K>, V <: Equatable<V>
```


### class AddressMap
包含地址信息映射。必须通过创建 不能直接构造

```cangjie
/**
 * 创建一个空 AddressMap 对象
 * 返回值 AddressMap - 返回一个空 AddressMap 对象
 */
public func newAddressMap(): AddressMap
```

```cangjie
public class AddressMap {
    /**
     * 设置一个 Address
     * 参数 addr - 设置的 Address
     * 参数 value - 设置对应的字符串值
     */
     public func set(addr: Address, value: String): Unit

    /**
     * 获取 Address 对应的值
     * 参数 addr - 要获取的 Address
     * 返回值 (String, Bool) - 值和是否成功获取
     */
     public func get(addr: Address): (String, Bool)

    /**
     * 移除一个 Address
     * 参数 addr - 要删除 Address
     */
     public func delete(addr: Address): Unit

    /**
     * 当前存入的 Address 数量
     * 返回值 Int64 - 返回大小
     */
     public func len(): Int64

    /**
     * 获取所有的 Address
     * 返回值 ArrayList<Address> - 返回 Address 列表
     */
     public func keys(): ArrayList<Address>
}
```

```cangjie
/**
 * 设置默认解析头
 * 参数 scheme - 设置默认解析头字符串
 */
public func setDefaultScheme(scheme: String)
```

```cangjie
/**
 * 获取默认解析头
 * 返回值 String - 返回当前解析头
 */
public func getDefaultScheme(): String
```

```cangjie
/**
 * 将一个构造器 Builder 注册进系统
 * 参数 b - 要注册的构造器
 */
public func register(b: Builder): Unit
```

```cangjie
/**
 * 根据解析获取一个构造器
 * 参数 scheme - 解析头字符串
 * 返回值 Builder - 返回对应的构造器，没有获取到则内部抛出异常
 */
public func get(scheme: String): Builder
```

```cangjie
/**
 * 根据解析头取消构造器的注册
 * 参数 scheme - 解析头字符串
 */
public func unregisterForTesting(scheme: String): Unit
```

### class Resolver
手动创建的解析器，可以通过 newBuilderWithScheme 创建

```cangjie
/**
 * 通过 scheme 名创建一个空 Resolver 对象。
 * 返回值 Resolver - 返回一个空 Resolver 对象
 */
public func newBuilderWithScheme(scheme: String): Resolver
```

```cangjie
public open class ManualResolver <: resolver.Builder & resolver.Resolver {
    /**
     * 初始化状态
     * 参数 scheme - 解析头字符串
     */
     public func initialState(s: resolver.State)

    /**
     * 传入解析的目标，构造解析器
     * 参数 target - 解析目标
     * 返回值 resolver.Resolver - 返回对应的解析器
     * 返回值 GrpcError - 返回错误信息
     */
     public func build(target: resolver.Target): (resolver.Resolver, GrpcError)

    /**
     * 返回当前解析器的头
     * 参数 String - 返回解析头字符串
     */
     public func scheme(): String

    /**
     * 用户传入自定义参数做自定义解析
     * 参数 o - ResolveNowOptions 类型的自定义参数
     */
     public func resolveNow(o: resolver.ResolveNowOptions): Unit

    /**
     * 调用用户自定义的 ClientConn 的 close
     */
     public func close(): Unit

    /**
     * 调用用户自定义的 ClientConn 的 updateState
     * 参数 resolver.State - 要处理的 State
     * 返回值 GrpcError - 返回一个 GrpcError
     */
     public func updateState(s: resolver.State): GrpcError

    /**
     * 处理 grpc 错误，调用用户自定义的 ClientConn 的 reportError
     * 参数 err - 要处理的 GrpcError
     */
     public func reportError(err: GrpcError): Unit
```

```cangjie
/**
 * 获取内置的 dns 解析器 Builder 用于设置域名解析。
 * 返回值 resolver.Builder - 返回一个 dns 解析器
 */
public func newDnsBuilder(): resolver.Builder
```

