/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package rpc4cj.attributes

internal import std.collection.*

/**
 * Attributes is an immutable struct for storing and retrieving generic
 * key/value pairs.  Keys must be hashable, and users should define their own
 * types for keys.  Values should not be modified after they are added to an
 * Attributes or if they were received from one.  If values implement 'Equal(o
 * interface{}) bool', it will be called by (*Attributes).Equal to determine
 * whether two values with the same key should be considered equal.
 */
public class Attributes<K,V> where K <: Hashable & Equatable<K>, V <: Equatable<V> {
    protected var m: HashMap<K,V> = HashMap<K,V>()

    /*
     * Equal returns whether a and o are equivalent.  If 'Equal(o interface{})
     * bool' is implemented for a value in the attributes, it is called to
     * determine if the value matches the one stored in the other attributes.  If
     * Equal is not implemented, standard equality is used to determine if the two
     * values are equal. Note that some types (e.g. maps) aren't comparable by
     * default, so they must be wrapped in a struct, or in an alias type, with Equal
     * defined. 
     */
    public func Equal(o: Attributes<K,V>): Bool {
        if (this.m.isEmpty() && o.m.isEmpty()) {
            return true
        }
        if (this.m.isEmpty() || o.m.isEmpty()) {
            return false
        }
        if (this.m.size != o.m.size) {
            return false
        }
        for ((k, v) in this.m) {
            if (!o.m.contains(k)) {
                // o missing element of a
                return false
            }

            if (v != o.m[k]) {
                return false
            }
        }
        return true
    }
}

/**
 * New returns a new Attributes containing the key/value pair.
 */
public func new<K,V>(key: K, value: V): Attributes<K,V> where K <: Hashable & Equatable<K>, V <: Equatable<V> {
    var attr = Attributes<K,V>()
    attr.m.add(key,value)
    return attr
}