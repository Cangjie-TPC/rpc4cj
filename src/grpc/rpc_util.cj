/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */

package rpc4cj.grpc

let EMPTY_ARRAY: Array<Byte> = Array<Byte>()

let maxInt: Int64 = Int64.Max

/*
 * Identifies an optional encoding that specifies an uncompressed stream.
 * This document is for grpc internal use only.
 */
let Identity: String = "identity"

public class ChannelzDataInside {
    protected var callsStarted: Int64 = 0
    protected var callsFailed: Int64 = 0
    protected var callsSucceeded: Int64 = 0

    /*
     * Stores the timestamp when the last call started.
     * It is of int64 type instead of time time,
     * because atomic updating of time time variables is more expensive than int64 variables.
     */
    protected var lastCallStartedTime: Int64 = 0
}

//The parser reads the complete gRPC message from the underlying reader.
public class Parser {

    //r is the underlying reader.
    protected var r: InputStream = ByteBuffer()

    //The title of the GRPC message.
    protected var header: Array<UInt8> = Array<UInt8>(5, repeat: 0)

    public init(r!: InputStream = ByteBuffer(), header!: Array<UInt8> = Array<UInt8>(5, repeat: 0)){
        this.r = r
        this.header = header
    }

    func recvMsg(maxReceiveMessageSize: Int64): (UInt8, Array<UInt8>, GrpcError) {

        var pf: UInt8 = 0
        var err: GrpcError = NULL_ERR
        var num: Int64 = 0
        try {
            if (this.header.size != 5) {
                throw Exception("this.header.size != 5, is wrong!")
            }
            num = this.r.read(this.header)
        } catch (e: Exception) {
            err = GrpcError(GrpcUnknown, e.message)
        }
        if (!err.isNull() || num == 0) {
            return (0, EMPTY_ARRAY_UINT8, err)
        }
        pf = this.header[0]
        let length: UInt32 = getUInt32(this.header[1..header.size])

        if (length == 0) {
            return (pf, EMPTY_ARRAY_UINT8, NULL_ERR)
        }

        if (Int64(length) > maxReceiveMessageSize) {
            return (0, EMPTY_ARRAY_UINT8, GrpcError(ResourceExhausted, "grpc: received message larger than max (${length} vs. ${maxReceiveMessageSize})"))
        }

        var msg: Array<UInt8> = Array<UInt8>(Int64(length), repeat: 0)
        try {
            num = this.r.read(msg)
        } catch (e: Exception) {
            err = GrpcError(GrpcUnknown, e.message)
        }
        if (num != Int64(length)) {
            err = GrpcError(GrpcUnknown, "The information read is error, length = ${length}, num = ${num}.")
        }
        if (!err.isNull()) {
            if (err is EOFError) {
                err = ErrUnexpectedEOF()
            }
            return (0, EMPTY_ARRAY_UINT8, err)
        }
        return (pf, msg, NULL_ERR)
    }
}

//Use the passed compressor or use the passed prepareMsg to return the hdr, payload, and data。
public func prepareMsg(m: MessageLite, codec: BaseCodec, flagCompress: Bool): (Array<Byte>, Array<Byte>, Array<Byte>, GrpcError) {

    //The input interface is not a prepared msg. Marshaling and compressing data at this time.
    let (data, err): (Array<Byte>, GrpcError) = encodeGrpc(codec, m)
    if (!err.isNull()) {
        return (EMPTY_ARRAY, EMPTY_ARRAY, EMPTY_ARRAY, err)
    }

    var compData: Array<Byte> = EMPTY_ARRAY

    if (flagCompress) {
        let (dataCompress, errCom): (Array<Byte>, GrpcError) = compressGRPC(data)

        if (!errCom.isNull()) {
            return (EMPTY_ARRAY, EMPTY_ARRAY, EMPTY_ARRAY, errCom)
        }
        compData = dataCompress
    }

    let (hdr, payload): (Array<Byte>, Array<Byte>) = msgHeader(data, compData)
    return (hdr, payload, data, NULL_ERR)
}

/*
 * Serialize the message and return the buffer containing the message.
 * If the message is too large to be transmitted by grpc, an error is returned.
 * If msg is nil, an empty message is generated.
 */
public func encodeGrpc(c: BaseCodec, msg: MessageLite): (Array<Byte>, GrpcError) {
    var data: Array<Byte> = EMPTY_ARRAY
    var err: GrpcError = NULL_ERR
    try {
        data = c.marshal((msg as MessageLite).getOrThrow())
    } catch (e: Exception) {
        err = GrpcError(GrpcUnknown, e.message)
    }
    if (!err.isNull()) {
        return (EMPTY_ARRAY, GrpcError(insideInternal, "grpc: error while marshaling: ${err.msg}"))
    }
    if (UInt64(data.size) > UInt64(UInt32.Max)) {
        return (EMPTY_ARRAY, GrpcError(ResourceExhausted, "grpc: message too large (${data.size} bytes)"))
    }
    return (data, NULL_ERR)
}

/*
 * Returns compressed input bytes.
 * Compress with CompressOutputStream of zlib library.
 * Unlike go, go uses a compressor that specifies the incoming.
 */
public func compressGRPC(inbuf: Array<Byte>): (Array<Byte>, GrpcError) {
    if(inbuf.size < 2) {
        return (inbuf, NULL_ERR)
    }

    var dest: Array<Byte> = EMPTY_ARRAY
    var err: GrpcError = NULL_ERR
    try {
        var buf: ByteBuffer = ByteBuffer(1024)
        var comp: CompressOutputStream = CompressOutputStream(buf, wrap: GzipFormat, bufLen: 1024)

        comp.write(inbuf)
        comp.flush()
        comp.close()

        dest = readToEnd(buf)
        buf.clear()
    } catch (e: Exception) {
        err = GrpcError(GrpcUnknown, e.message)
    }
    return (dest, err)
}

/*
 * A 5-byte header is returned for the message and payload being transmitted.
 * If it is not nil, it is compData; otherwise, it is data.
 * 5 bytes of the header. The first byte indicates whether to compress, 0-uncompressed, 1-compressed.
 * 2-5 bytes represent the length of transmitted data.
 */
public func msgHeader(buf: Array<Byte>, compData: Array<Byte>): (Array<Byte>, Array<Byte>) {
    var data: Array<Byte> = buf
    let hdr: Array<Byte> = Array<Byte>(Int64(HEADER_LEN), repeat: 0)
    if (compData.size != 0) {
        hdr[0] = COMPRESSION_MADE
        data = compData
    } else {
        hdr[0] = COMPRESSION_NONE
    }
    putUInt32(hdr, UInt32(data.size), 1)
    return (hdr, data)
}

func checkRecvPayload(pf: UInt8, recvCompress: String): GrpcError {
    match {
        case pf == COMPRESSION_NONE => ()
        case pf == COMPRESSION_MADE =>
            if (recvCompress.isEmpty() || recvCompress == Identity) {
                return GrpcError(insideInternal, "grpc: compressed flag set with identity or empty encoding")
            }
        case _ =>
            return GrpcError(insideInternal, "grpc: received unexpected payload format ${pf}")
    }
    return NULL_ERR
}

public class PayloadInfo {
    //The compressed length obtained from the wire.
    protected var wireLength: Int64 = 0
    protected var uncompressedBytes: Array<UInt8> = EMPTY_ARRAY_UINT8

    protected var isNone: Bool = false

    public init() {
        this.isNone = true
    }

    public init(wireLength: Int64) {
        this.wireLength = wireLength
    }

    public func isNull(): Bool {
        return this.isNone
    }
}


//Use the standard library zlib library compression and decompression tool.
public func recvAndDecompress(p: Parser, s: Stream, maxReceiveMessageSize: Int64,
                        payInfo: PayloadInfo): (Array<UInt8>, GrpcError) {
    var buf: Array<Byte> = EMPTY_ARRAY
    let (pf, dbuf, err): (UInt8, Array<UInt8>, GrpcError)= p.recvMsg(maxReceiveMessageSize)
    if (!err.isNull()) {
        return (EMPTY_ARRAY_UINT8, err)
    }
    if (!payInfo.isNull()) {
        payInfo.wireLength = dbuf.size
    }
    buf = dbuf

    let st: GrpcError = checkRecvPayload(pf, s.getRecvCompress())
    if (!st.isNull()) {
        return (EMPTY_ARRAY_UINT8, st)
    }

    if (pf == COMPRESSION_MADE) {
        let (dbuf2, size, errDC): (Array<Byte>, Int64, GrpcError) = decompress(dbuf, maxReceiveMessageSize)
        buf = dbuf2
        if (!errDC.isNull()) {
            return (EMPTY_ARRAY_UINT8, GrpcError(insideInternal, "grpc: failed to decompress the received message ${errDC.msg}"))
        }
        if (size > maxReceiveMessageSize) {
            return (EMPTY_ARRAY_UINT8, GrpcError(ResourceExhausted, "grpc: received message after decompression larger than max (${size} vs. ${maxReceiveMessageSize})"))
        }
    }
    return (buf, NULL_ERR)
}

/*
 * Use the compressor to decompress d and return data and size.
 * Or, if the data will exceed maxReceiveMessageSize, only the size will be returned.
 */
public func decompress(dcbuf: Array<UInt8>, maxReceiveMessageSize: Int64): (Array<Byte>, Int64, GrpcError) {
    var data: Array<Byte> = EMPTY_ARRAY
    var err: GrpcError = NULL_ERR
    var sum: Int64 = 0
    try {
        var buf: ByteBuffer = ByteBuffer(1024)
        var dc: DecompressOutputStream = DecompressOutputStream(buf, wrap: GzipFormat, bufLen: 1024)

        dc.write(dcbuf)
        dc.flush()
        dc.close()

        data = readToEnd(buf)
        sum = data.size
        buf.clear()
    } catch (e: Exception) {
        err = GrpcError(GrpcUnknown, e.message)
    }

    if (!err.isNull()) {
        return (EMPTY_ARRAY_UINT8, 0, err)
    }
    if (sum > maxReceiveMessageSize) {
        return (EMPTY_ARRAY_UINT8, sum, NULL_ERR)
    }
    return (data, sum, err)
}


//Use the standard library zlib library compression and decompression tool.
public func recv(p: Parser, c: BaseCodec, s: Stream, m: MessageLite, maxReceiveMessageSize: Int64, payInfo: PayloadInfo): GrpcError {
    let (dbuf, err): (Array<UInt8>, GrpcError) = recvAndDecompress(p, s, maxReceiveMessageSize, payInfo)
    if (!err.isNull()) {
        return err
    }
    var errum: GrpcError = NULL_ERR
    try {
        c.unmarshal(ArrayList<UInt8>(dbuf), m)
    } catch (e: Exception) {
        errum = GrpcError(GrpcUnknown, e.message)
    }

    if (!errum.isNull()) {
        return GrpcError(insideInternal, "grpc: failed to unmarshal the received message ${errum.msg}")
    }
    if (!payInfo.isNull()) {
        payInfo.uncompressedBytes = dbuf
    }
    return NULL_ERR
}

//Convert errors to errors in the status package.
public func toRPCErr(err: GrpcError): GrpcError {
    if (err.isNull() || err is EOFError) {
        return err
    } else if (err is ErrUnexpectedEOF) {
        return GrpcError(insideInternal, err.msg)
    }

    if (err is ConnectionError) {
        return GrpcError(DeadlineExceeded, err.msg)
    } else if (err is NewStreamError) {
        let nerr: NewStreamError = (err as NewStreamError).getOrThrow()
        toRPCErr(nerr.err)
    }

    return GrpcError(GrpcUnknown, err.msg)
}