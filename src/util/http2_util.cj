/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */

package rpc4cj.util

internal import std.time.*
internal import std.io.*
internal import std.sync.*
internal import std.collection.*
internal import std.net.*
internal import net.http.*
internal import encoding.base64.*
internal import std.convert.*

internal import rpc4cj.http2.hpack.HeaderField

internal import rpc4cj.exceptions.*
internal import rpc4cj.buffer.*

//public let TIME_UTC_ZONE: DateTime = DateTime.of(month: Month.January, timeZone: Location.UTC)
public let TIME_UTC_ZONE: DateTime = DateTime.of(year: 1,month: Month.January,dayOfMonth: 1,timeZone: TimeZone.UTC)



//Nanoseconds elapsed since UTC on January 1, 1970(1970.01.01)
public func unixNano(): Int64 {
    return DateTime.now().toUnixTimeStamp().toNanoseconds()
}

public func validStreamID(streamID: UInt32): Bool {
    return streamID != 0 && (streamID & (1<<31)) == 0
}

public func validWireHeaderFieldName(v: String): Bool {
    if (v.isEmpty()) {
        return false
    }
    for (r in v.toRuneArray()) {
        if (!isTokenRune(Int64(UInt32(r)))) {
            return false
        }
        if (r'A' <= r && r <= r'Z') {
            return false
        }
    }
    return true
}


public func handleContentSubtype(contentType: String): (String, Bool) {
    if (contentType == baseContentType) {
        return ("", true)
    }
    //The (k, v) of the transmitted header contains control characters after parsing
    if(contentType.contains(baseContentType)) {
        let res: String = clearHeadAndTailControlCharacters(contentType)
        if (res == baseContentType) {
            return ("", true)
        }
    }
    if (!contentType.startsWith(baseContentType)) {
        return ("", false)
    }
    // guaranteed since != baseContentType and has baseContentType prefix
    let charContentType: Array<Rune> = contentType.toRuneArray()
    let len: Int64 = baseContentType.toRuneArray().size
    let char: Rune = charContentType[len]
    if (char == r'+' || char == r';') {
        /*
         * For "application/grpc+" or "application/grpc;", This will return true
         * The previous validContentType function was tested as valid,
         * so we only said that no content subtype was specified in this case.
         */
        return (String(charContentType[(len + 1) .. charContentType.size]), true)
    } else {
        return ("", false)
    }
}

public func clearHeadAndTailControlCharacters(contentType: String): String {
    if (!hasNotPrintable(contentType)) {
        return contentType
    }
    let arr: Array<UInt8> = contentType.toArray()
    let ctSize: Int64 = arr.size
    let res: Array<UInt8> = Array<UInt8>(ctSize, repeat: 0)
    var index: Int64 = 0
    for(i in 0..ctSize) {
        let num: UInt8 = arr[i]
        if(num >= 32 && num < 127) {
            res[index] = num
            index++
        }
    }
    return String.fromUtf8(res[0..index])
}

//If msg contains any characters not in 0x20-0x7E(32-126)/(' ' - '~'), return true
public func hasNotPrintable(msg: String): Bool {
    for (c in msg.toRuneArray()) {
        if (c < r' ' || c > r'~') {
            return true
        }
    }
    return false
}

public func appendHashMapValue(map: HashMap<String, Array<String>>, name: String, value: String): Unit {
    if (map.contains(name)) {
        let arr: Array<String> = map[name]
        if (arr.size == 0) {
            map.add(name, [value])
        } else {
            let rst: Array<String> = Array<String>(arr.size + 1, repeat: "")
            arr.copyTo(rst, 0, 0, arr.size)
            rst[arr.size] = value
            map.add(name, rst)
        }
    } else {
        map.add(name, [value])
    }
}

var isTokenTables: Array<Bool> = [
    false,
    false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false,
    false, false,
    true, false, true, true, true, true, true, false,
    false, true, true, false, true, true, false,
    true, true, true, true, true, true, true, true, true, true,
    false, false, false, false, false, false, false,
    true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
    false, false, false, true, true, true,
    true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
    false, true, false, true, false
]

let IS_TOKEN_TABLE_SIZE: Int64 = 127

func isTokenRune(r: Int64): Bool {
    return r < isTokenTables.size && isTokenTables[r]
}
