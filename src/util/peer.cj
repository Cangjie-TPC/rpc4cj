/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */

/**
 * @file
 * The file declars the class Peer.
 */
package rpc4cj.util

/**
 * Peer contains RPC peer information, such as address and authentication information.
 * @author lipei
 * @since 0.32.5
 */
public class Peer {

    //Is the peer address.
    public var addr: SocketAddress

    //Is the authentication information transmitted. Zero if transport security is not used.
    public var authInfo: Option<AuthInfo> = None

    /**
     * The Function is init constructor
     *
     * @param addr of SocketAddress, and the Default value is EMPTY_ADDR
     * @param authInfo of Option<AuthInfo>, and the Default value is None
     * @since 0.32.5
     */
    public init(addr!: SocketAddress = EMPTY_ADDR, authInfo!: Option<AuthInfo> = None) {
        this.addr = addr
        this.authInfo = authInfo
    }
}
