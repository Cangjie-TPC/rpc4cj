/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package rpc4cj.exceptions

public let OK: UInt32 = 0
public let Canceled: UInt32 = 1
public let GrpcUnknown: UInt32 = 2
public let InvalidArgument: UInt32 = 3
public let DeadlineExceeded: UInt32 = 4
public let NotFound: UInt32 = 5
public let AlreadyExists: UInt32 = 6
public let PermissionDenied: UInt32 = 7
public let ResourceExhausted: UInt32 = 8
public let FailedPrecondition: UInt32 = 9
public let Aborted: UInt32 = 10
public let OutOfRange: UInt32 = 11
public let Unimplemented: UInt32 = 12
public let insideInternal: UInt32 = 13
public let Unavailable: UInt32 = 14
public let DataLoss: UInt32 = 15
public let Unauthenticated: UInt32 = 16

public let MaxCode: UInt32 = 17

var strToCode: Array<String> = [
    "OK",                  // OK = 0
    "CANCELLED",           // Canceled = 1
    "UNKNOWN",             // Unknown = 2
    "INVALID_ARGUMENT",    // InvalidArgument = 3
    "DEADLINE_EXCEEDED",   // DeadlineExceeded = 4
    "NOT_FOUND",           // NotFound = 5
    "ALREADY_EXISTS",      // AlreadyExists = 6
    "PERMISSION_DENIED",   // PermissionDenied = 7
    "RESOURCE_EXHAUSTED",  // ResourceExhausted = 8
    "FAILED_PRECONDITION", // FailedPrecondition = 9
    "ABORTED",             // Aborted = 10
    "OUT_OF_RANGE",        // OutOfRange = 11
    "UNIMPLEMENTED",       // Unimplemented = 12
    "INTERNAL",            // Internal = 13
    "UNAVAILABLE",         // Unavailable = 14
    "DATA_LOSS",           // DataLoss = 15
    "UNAUTHENTICATED"      // Unauthenticated = 16
]

foreign func htons(port: UInt16): UInt16
public func portTrans(port: UInt16): UInt16 {
    unsafe { htons(port) }
}

