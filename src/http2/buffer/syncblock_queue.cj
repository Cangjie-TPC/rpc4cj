/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */


/**
 * @file The file declars the SyncQueue class.
 */

package rpc4cj.http2.buffer

/**
 * The class is SyncQueue<T>
 * @since
 */
public class SyncQueue<T> {
    private let maxQueueMemory: Int64

    private var list: LinkedListHttp2<T> = LinkedListHttp2<T>()

    private var notFull: ConditionID
    private var notEmpty: ConditionID
    private let m: MultiConditionMonitor = MultiConditionMonitor()

    /**
     * The Function is init constructor
     *
     * @param The param is size of Int64
     *
     * @since
     */
    public init(size: Int64) {
        this.maxQueueMemory = size
        synchronized(m) {
            notFull = m.newCondition()
            notEmpty = m.newCondition()
        }
    }

    /**
     * The Function is init constructor
     *
     * @since
     */
    public init() {
        this.maxQueueMemory = 8192
        synchronized(m) {
            notFull = m.newCondition()
            notEmpty = m.newCondition()
        }
    }

    /**
     * The Function is add
     *
     * @param The param is item of T
     *
     * @since
     */
    public func put(item: T) {
        synchronized(m) {
            while (this.list.size == this.maxQueueMemory){
                m.wait(notFull)
            }
            this.list.append(item)
            m.notify(notEmpty)
        }
    }

    /**
     * The Function is pull
     *
     * @return The return type of Option<T>
     * @since
     */
    public func poll(): T {
        synchronized(m) {
            while (this.list.size == 0){
                m.wait(notEmpty)
            }
            let ret = this.list.removeFirst()
            m.notify(notFull)
            return ret
        }
    }

    /**
     * The Function is recycle
     *
     * @since
     */
    public func recycle() {
        synchronized(m) {
            this.list.clear()
        }
    }
}

