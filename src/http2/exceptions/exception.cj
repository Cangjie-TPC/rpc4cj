/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */

/**
 * @file The file declars the Http2Exception class.
 */

package rpc4cj.http2.exceptions


/**
 * The type Hpack exception.
 */
public open class Http2Exception <: Exception {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

public open class Http2PingException <: Http2Exception {
    public init(){
        super()
    }

    public init(string: String) {
        super(string)
    }
}

/**
 * The type Hpack exception.
 */
public open class HpackException <: Exception {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

public open class InvalidHuffmanCodeException <: Exception {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

/**
 * The class is HpackStaticTableOutOfBoundsException inherited from HpackException
 * @author liyanqing14
 * @since 0.29.3
 */
public class HpackStaticTableOutOfBoundsException <: HpackException {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

/**
 * The class is HpackDynamicTableOutOfBoundsException inherited from HpackException
 * @author liyanqing14
 * @since 0.29.3
 */
public class HpackDynamicTableOutOfBoundsException <: HpackException {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

public class DynamicTableSizeUpdateException <: HpackException {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }
}

public class HpackDecodeException <: HpackException {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }
}

/**
 * The class is StateException inherited from RuntimeException
 * @since 0.29.3
 */
public open class StateException <: Exception {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     *
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

/**
 * The class is CnnectionStateException inherited from StateException
 * @since 0.29.3
 */
public class CnnectionStateException <: StateException {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

/**
 * The class is StreamStateException inherited from StateException
 * @since 0.29.3
 */
public class StreamStateException <: StateException {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        super()
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super(string)
    }
}

public class FramesEncoderException <: Exception {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        this("FramesEncoderException: ")
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super("FramesEncoderException: ${string}")
    }
}

public class FramesDecoderException <: Exception {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        this("FramesDecoderException: ")
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super("FramesDecoderException: ${string}")
    }
}

    public class InterruptedException <: Exception {
    /**
     * The Function is init constructor
     *
     * @since 0.29.3
     */
    public init(){
        this("InterruptedException: ")
    }

    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.29.3
     */
    public init(string: String) {
        super("InterruptedException: ${string}")
    }
}

public class ReadException <: Exception {

    /**
     * The Function is init constructor
     *
     * @since 0.35.6
     */
    public init(){
        this("")
    }
    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.35.6
     */
    public init(string: String) {
        super("ReadException: \n${string}")
    }
}

/**
 * @file
 * The file declars the WriteException class.
 */

public class WriteException <: Exception {

    /**
     * The Function is init constructor
     *
     * @since 0.35.6
     */
    public init(){
        this("")
    }
    /**
     * The Function is init constructor
     *
     * @param string of String
     * @since 0.35.6
     */
    public init(string: String) {
        super("WriteException: \n${string}")
    }
}
